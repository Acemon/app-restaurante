package com.fernando.restaurantesdezaragoza;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fernando on 18/12/2016.
 */
public class OpinionAdapter extends BaseAdapter {
    private Context contexto;
    private List<Opinion> opiniones;
    private LayoutInflater inflater;

    public OpinionAdapter(Context contexto, List<Opinion> opiniones) {
        this.contexto = contexto;
        this.opiniones = opiniones;
        this.inflater = LayoutInflater.from(contexto);
    }

    @Override
    public int getCount() {
        return opiniones.size();
    }

    @Override
    public Object getItem(int position) {
        return opiniones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder2 holder = null;

        if (convertView==null){
            convertView = inflater.inflate(R.layout.opinion, null);
            holder = new ViewHolder2();
            holder.titulo = (TextView) convertView.findViewById(R.id.tvTituloopinion);
            holder.comentario = (TextView) convertView.findViewById(R.id.tvComentarioopinion);
            holder.user = (TextView) convertView.findViewById(R.id.tvUseropinion);
            holder.rate = (TextView) convertView.findViewById(R.id.tvRateOpinion);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder2) convertView.getTag();
        }
        Opinion opinion = opiniones.get(position);
        holder.titulo.setText(opinion.getTitulo());
        holder.comentario.setText(opinion.getTexto());
        holder.user.setText(opinion.getUsuario());
        holder.rate.setText(String.valueOf(opinion.getPuntuacion()));

        return convertView;
    }
    static class ViewHolder2{
        TextView titulo;
        TextView comentario;
        TextView user;
        TextView rate;
    }
}
