package com.fernando.restaurantesdezaragoza;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by DAM on 14/12/2016.
 */
public class Restaurante {
    private String nombre;
    private String url;
    private String descripcion;
    private String categoria;
    private Double[] coordenadas;

    public Restaurante() {
        nombre = "";
        url = "";
        descripcion = "";
        categoria = "";
        coordenadas = new Double[2];
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Double[] getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(Double[] coordenadas) {
        this.coordenadas = coordenadas;
    }

    @Override
    public String toString() {
        return "Restaurante{" +
                "nombre='" + nombre + '\'' +
                ", url='" + url + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", categoria='" + categoria + '\'' +
                ", coordenadas=" + Arrays.toString(coordenadas) +
                '}';
    }
}
