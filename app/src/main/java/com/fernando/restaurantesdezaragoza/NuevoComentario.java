package com.fernando.restaurantesdezaragoza;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class NuevoComentario extends AppCompatActivity implements View.OnClickListener {
    private Opinion opinion;
    String restaurante;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_comentario);

        Intent intent = getIntent();
        restaurante = intent.getStringExtra("Restaurante");

        opinion=null;
        Button btValidar = (Button) findViewById(R.id.btValidar);
        btValidar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        opinion= new Opinion();
        SharedPreferences usuario = PreferenceManager.getDefaultSharedPreferences(this);

        EditText etNuevoTitulo = (EditText) findViewById(R.id.etNuevoTitulo);
        EditText etNuevoComentario = (EditText) findViewById(R.id.etNuevoComentario);
        RatingBar barraPuntuaciones = (RatingBar) findViewById(R.id.barraPuntuacion);
        opinion.setTitulo(etNuevoTitulo.getText().toString());
        opinion.setTexto(etNuevoComentario.getText().toString());
        opinion.setPuntuacion(barraPuntuaciones.getRating());
        opinion.setUsuario(usuario.getString("nombre", "Anon"));
        opinion.setRestaurante(restaurante);

        WebService webService = new WebService();
        webService.execute();
    }
    private class WebService extends AsyncTask<String, Void, Void> {
        private ProgressDialog dialog;
        @Override
        protected Void doInBackground(String... params) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            // FIXME
            //http://192.168.1.34:8080/add_opinion?titulo=djdjdjdj&texto=gsgsgs&puntuacion=5&restaurante=A%20MESA%20PUESTA&usuraio=Anon
            restTemplate.getForObject(Constantes.URL+
                    "/add_opinion?titulo="+opinion.getTitulo()+
                    "&texto="+opinion.getTexto()+
                    "&puntuacion="+opinion.getPuntuacion()+
                    "&restaurante="+opinion.getRestaurante()+
                    "&usuario="+opinion.getUsuario(),
                    Void.class);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(NuevoComentario.this);
            dialog.setTitle(R.string.enviado);
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (dialog!= null)
                dialog.dismiss();
            EditText etNuevoTitulo = (EditText) findViewById(R.id.etNuevoTitulo);
            etNuevoTitulo.setText("");
            EditText etNuevoComentario = (EditText) findViewById(R.id.etNuevoComentario);
            etNuevoComentario.setText("");
            RatingBar barraPuntuaciones = (RatingBar) findViewById(R.id.barraPuntuacion);
            barraPuntuaciones.setNumStars(0);
        }
    }
}
