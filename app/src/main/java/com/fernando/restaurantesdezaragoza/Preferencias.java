package com.fernando.restaurantesdezaragoza;

import android.preference.PreferenceActivity;
import android.os.Bundle;

import com.fernando.restaurantesdezaragoza.R;

public class Preferencias extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferencias);
    }
}