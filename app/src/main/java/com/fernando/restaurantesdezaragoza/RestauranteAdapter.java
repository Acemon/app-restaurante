package com.fernando.restaurantesdezaragoza;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by DAM on 14/12/2016.
 */
public class RestauranteAdapter extends BaseAdapter{

    private Context contexto;
    private ArrayList<Restaurante> restaurantes;
    private LayoutInflater inflater;

    public RestauranteAdapter(Context contexto, ArrayList<Restaurante> restaurantes) {
        this.contexto=contexto;
        this.restaurantes=restaurantes;
        this.inflater=LayoutInflater.from(this.contexto);
    }

    @Override
    public int getCount() {
        return restaurantes.size();
    }

    @Override
    public Object getItem(int i) {
        return restaurantes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;

        if (convertView == null){
            convertView = inflater.inflate(R.layout.restaurante, null);

            holder = new ViewHolder();
            holder.foto = (ImageView) convertView.findViewById(R.id.ivFoto);
            holder.nombre = (TextView) convertView.findViewById(R.id.tvNombre);
            holder.categoria = (TextView) convertView.findViewById(R.id.tvCategoria);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        Restaurante restaurante = restaurantes.get(i);
        holder.foto.setImageDrawable(contexto.getResources().getDrawable(R.drawable.restaurante));
        holder.nombre.setText(restaurante.getNombre());
        holder.categoria.setText(restaurante.getCategoria());

        return convertView;
    }

    static class ViewHolder {
        ImageView foto;
        TextView nombre;
        TextView categoria;
    }
}
