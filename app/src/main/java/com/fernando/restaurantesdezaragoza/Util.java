package com.fernando.restaurantesdezaragoza;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

/**
 * Created by Fernando on 18/12/2016.
 */
public class Util {
    public static byte[] getBytes(Bitmap imagen) {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        imagen.compress(Bitmap.CompressFormat.JPEG, 0, bos);
        return bos.toByteArray();
    }

    public static Bitmap getBitmap(byte[] imagen) {

        return BitmapFactory.decodeByteArray(imagen, 0, imagen.length);
    }
}
