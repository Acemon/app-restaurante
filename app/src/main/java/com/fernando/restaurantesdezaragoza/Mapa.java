package com.fernando.restaurantesdezaragoza;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.services.Constants;
import com.mapbox.services.commons.ServicesException;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;
import com.mapbox.services.directions.v5.DirectionsCriteria;
import com.mapbox.services.directions.v5.MapboxDirections;
import com.mapbox.services.directions.v5.models.DirectionsResponse;
import com.mapbox.services.directions.v5.models.DirectionsRoute;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Mapa extends Activity {
    private MapView mapaView;
    private MapboxMap mapa;
    private String restaurante;
    private String categoria;
    private String descripcion;
    private Double[] coordenadas;
    LocationServices servicioUbicacion;
    private Location lastLocation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapboxAccountManager.start
                (this, "pk.eyJ1IjoiZmVybmFuZG96Z3oiLCJhIjoiY2l3c2RuNWlkMHl0azJ5bW9lZTI3YnZweCJ9._bHS1wOSjETnpK5Vqvq0UQ");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_mapa);

        //Notificacion
        NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(Mapa.this)
                .setContentTitle("Restaurante")
                .setContentText("Estas mirando el mapa")
                .setSmallIcon(R.drawable.default_marker);

        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(0, nBuilder.build());

        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, 1);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.INTERNET}, 1);

        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final Intent intent = getIntent();
        categoria=intent.getStringExtra("categoria");
        restaurante=intent.getStringExtra("restaurante");
        descripcion=intent.getStringExtra("descripcion");
        coordenadas=new Double[]{
                intent.getDoubleExtra("coordenadas1",0),
                intent.getDoubleExtra("coordenadas2",0)
        };

        mapaView = (MapView) findViewById(R.id.mapaView);
        mapaView.onCreate(savedInstanceState);
        mapaView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapa = mapboxMap;
                mapboxMap.addMarker(new MarkerOptions()
                    .position(new LatLng(coordenadas[1],coordenadas[0]))
                    .title(restaurante)
                    .snippet(categoria+"\n"+descripcion)
                );
                CameraPosition posicion = new CameraPosition.Builder()
                        .target(new LatLng(coordenadas[1], coordenadas[0]))
                        .zoom(13) // Zoom
                        .tilt(25) // Inclinacion camara
                        .build();
                mapboxMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(posicion), 7000);
                ubicarUsuario();
            }
        });

    }

    private void ubicarUsuario() {
        LocationServices servicioUbicacion = LocationServices.getLocationServices(this);
        Location lastLocation = null;
        if (mapa != null) {
            lastLocation = servicioUbicacion.getLastLocation();
            if (lastLocation != null)
                mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation), 16));

            mapa.setMyLocationEnabled(true);
        }
        Location loc = new Location("asdas");
        loc.setLatitude(coordenadas[0]);
        loc.setLongitude(coordenadas[1]);

//        try {
//            obtenerRuta(loc, lastLocation);
//        } catch (ServicesException e) {
//            e.printStackTrace();
//        }
    }

    private void obtenerRuta(Location markerLocation, Location userLocation) throws ServicesException {
        Position posicionMarker = Position.fromCoordinates(markerLocation.getLongitude(), markerLocation.getLatitude());
        Position posicionUsuario = Position.fromCoordinates(userLocation.getLongitude(), userLocation.getLatitude());

        MapboxDirections direccion = new MapboxDirections.Builder()
                .setOrigin(posicionMarker)
                .setDestination(posicionUsuario)
                .setProfile(DirectionsCriteria.PROFILE_CYCLING)
                .setAccessToken(MapboxAccountManager.getInstance().getAccessToken())
                .build();

        direccion.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                DirectionsRoute ruta = response.body().getRoutes().get(0);
                pintarRuta(ruta);
            }

            @Override
            public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                Toast.makeText(Mapa.this, "Error al generar una ruta", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void pintarRuta(DirectionsRoute ruta) {
        LineString lineString = LineString.fromPolyline(ruta.getGeometry(), Constants.OSRM_PRECISION_V5);
        List<Position> coordenadas = lineString.getCoordinates();
        LatLng[] puntos = new LatLng[coordenadas.size()];
        for (int i = 0; i < coordenadas.size(); i++) {
            puntos[i] = new LatLng(coordenadas.get(i).getLatitude(), coordenadas.get(i).getLongitude());
        }
        mapa.addPolyline(new PolylineOptions()
                .add(puntos)
                .color(Color.GREEN)
                .width(5));
        if (!mapa.isMyLocationEnabled())
            mapa.setMyLocationEnabled(true);
    }
}
