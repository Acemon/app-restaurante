package com.fernando.restaurantesdezaragoza;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class Detalles extends AppCompatActivity implements View.OnClickListener {
    private Restaurante restaurante;
    private Double[] coordenadas;
    private BaseDatos bbdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);
        bbdd = new BaseDatos(this);
        restaurante = new Restaurante();
        TextView tvNombre = (TextView) findViewById(R.id.tvdetRestaurante);
        TextView tvCategoria = (TextView) findViewById(R.id.tvdetCategoria);
        TextView tvCalle = (TextView) findViewById(R.id.tvdetCalle);
        Button btLocalizar = (Button) findViewById(R.id.btLocalizar);
        Button btComentarios = (Button) findViewById(R.id.btComentarios);
        Switch swFavorito = (Switch) findViewById(R.id.swFavorito);

        Intent intent = getIntent();
        restaurante.setNombre(intent.getStringExtra("restaurante"));
        tvNombre.setText(restaurante.getNombre());
        restaurante.setCategoria(intent.getStringExtra("categoria"));
        tvCategoria.setText(restaurante.getCategoria());
        restaurante.setDescripcion(intent.getStringExtra("descripcion"));
        tvCalle.setText(restaurante.getDescripcion());
        coordenadas=new Double[]{
                intent.getDoubleExtra("coordenadas1",0),
                intent.getDoubleExtra("coordenadas2",0)
        };
        restaurante.setCoordenadas(coordenadas);
        restaurante.setUrl(intent.getStringExtra("url"));


        Boolean b=bbdd.validarFavorito(restaurante.getNombre());
        swFavorito.setChecked(b);
        btLocalizar.setOnClickListener(this);
        btComentarios.setOnClickListener(this);
        swFavorito.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    bbdd.nuevoRestaurante(restaurante);
                }else{
                    bbdd.eliminarRestaurante(restaurante);
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.resources:
                intent = new Intent(this, Preferencias.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btLocalizar:
                // Abrir mapa
                Intent intent = new Intent(this, Mapa.class);
                intent.putExtra("restaurante", restaurante.getNombre());
                intent.putExtra("categoria", restaurante.getCategoria());
                intent.putExtra("descripcion", restaurante.getDescripcion());
                intent.putExtra("coordenadas1", coordenadas[0]);
                intent.putExtra("coordenadas2", coordenadas[1]);
                startActivity(intent);
                break;
            case R.id.btComentarios:
                //Conexion a Spring
                Intent intento = new Intent(this, Comentarios.class);
                intento.putExtra("restaurante", restaurante.getNombre());
                startActivity(intento);
                break;
        }
    }
}