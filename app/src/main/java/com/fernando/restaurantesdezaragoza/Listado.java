package com.fernando.restaurantesdezaragoza;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;


public class Listado extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ArrayList<Restaurante> restaurantes;
    private RestauranteAdapter adaptador1;
    private BaseDatos bbdd;
    SharedPreferences preferencias;
    boolean favoritos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);
        restaurantes = new ArrayList<>();
        preferencias = PreferenceManager.getDefaultSharedPreferences(this);
        bbdd = new BaseDatos(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        favoritos = preferencias.getBoolean("Favoritos",false);
        restaurantes.clear();

        adaptador1 = new RestauranteAdapter(this, restaurantes);
        ListView lvRestaurantes = (ListView) findViewById(R.id.lvRestaurantes);
        lvRestaurantes.setAdapter(adaptador1);
        lvRestaurantes.setOnItemClickListener(this);
        if (favoritos){
            restaurantes.addAll(bbdd.obtenerRestaurantes());
            adaptador1.notifyDataSetChanged();
        }else{
            // Iniciar Descarga
            descargarRestaurantes descarga = new descargarRestaurantes();
            descarga.execute(Constantes.Direccion);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.resources:
                intent = new Intent(this, Preferencias.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Restaurante restaurante = restaurantes.get(i);
        Intent intent = new Intent(this, Detalles.class);
        intent.putExtra("restaurante", restaurante.getNombre());
        intent.putExtra("url", restaurante.getUrl());
        intent.putExtra("categoria", restaurante.getCategoria());
        intent.putExtra("descripcion", restaurante.getDescripcion());
        intent.putExtra("coordenadas1", restaurante.getCoordenadas()[0]);
        intent.putExtra("coordenadas2", restaurante.getCoordenadas()[1]);
        startActivity(intent);
    }

    private class descargarRestaurantes extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... urls) {
            String url = urls[0];
            InputStream is = null;
            String resultado = null;
            JSONObject json = null;
            JSONArray jsonArray = null;

            try {
                // Conecta URL y obtiene datos
                HttpClient clienteHttp = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
                HttpResponse respuesta = clienteHttp.execute(httpGet);
                HttpEntity entity = respuesta.getEntity();
                is = entity.getContent();

                // leer + cadena texto
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String linea = null;

                while ((linea = br.readLine()) != null)
                    sb.append(linea + "\n");

                is.close();
                resultado = sb.toString();

                json = new JSONObject(resultado);
                jsonArray = json.getJSONArray("features");

                String nombre;
                String url2;
                String descripcion;
                String categoria;
                Double[] coordenadas;
                Restaurante restaurante;

                for (int i=0; i<jsonArray.length(); i++){
                    //info:
                    //http://jsonviewer.stack.hu/#http://www.zaragoza.es/georref/json/hilo/ver_Restaurante?srsname=wgs84&georss_valid=s

                    nombre = jsonArray.getJSONObject(i).getJSONObject("properties").getString("title");
                    url2 = jsonArray.getJSONObject(i).getJSONObject("properties").getString("link");
                    descripcion = jsonArray.getJSONObject(i).getJSONObject("properties").getString("description");
                    categoria = jsonArray.getJSONObject(i).getJSONObject("properties").getString("category");
                    String coordenadasGene = jsonArray.getJSONObject(i).getJSONObject("geometry").getString("coordinates");
                    String[] arrayCoordenadas = coordenadasGene.substring(1, coordenadasGene.length() - 1).split(",");
                    coordenadas = new Double[]{
                        Double.parseDouble(arrayCoordenadas[0]),
                        Double.parseDouble(arrayCoordenadas[1])
                    };
                    restaurante = new Restaurante();
                    restaurante.setNombre(nombre);
                    restaurante.setUrl(url2);
                    restaurante.setDescripcion(descripcion);
                    restaurante.setCategoria(categoria);
                    restaurante.setCoordenadas(coordenadas);
                    restaurantes.add(restaurante);

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

            adaptador1.notifyDataSetChanged();
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            adaptador1.notifyDataSetChanged();
        }
    }
}
