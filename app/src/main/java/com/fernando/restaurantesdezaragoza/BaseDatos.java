package com.fernando.restaurantesdezaragoza;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by DAM on 15/12/2016.
 */
public class BaseDatos extends SQLiteOpenHelper {
    private static final String NOMBRE = "RestaurantesZaragoza.db";
    private static final int VERSION = 1;
    private static final String TABLA_RESTAURANTES = "restaurantes";
    private static final String TABLA_USUARIO = "usuario";

    public BaseDatos(Context context) {
        super(context, NOMBRE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + TABLA_RESTAURANTES +
                "(nombre TEXT PRIMARY KEY," +
                "descripcion TEXT," +
                "categoria TEXT," +
                "direccion TEXT," +
                "coordenada1 DOUBLE," +
                "coordenada2 DOUBLE);");

        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + TABLA_USUARIO +
                "(usuario TEXT PRIMARY KEY," +
                "foto BLOB);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void nuevoRestaurante (Restaurante restaurante){
        SQLiteDatabase db=null;
        try{
            db = getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("nombre", restaurante.getNombre());
            values.put("descripcion", restaurante.getDescripcion());
            values.put("categoria", restaurante.getCategoria());
            values.put("direccion", restaurante.getUrl());
            values.put("coordenada1", restaurante.getCoordenadas()[0]);
            values.put("coordenada2", restaurante.getCoordenadas()[1]);

            db.insertOrThrow(TABLA_RESTAURANTES, null, values);
        }catch (SQLiteConstraintException e){
            Log.d("Repetido", e.getMessage());
        }finally {
            if (db != null)
                db.close();
        }

    }

    public ArrayList<Restaurante> obtenerRestaurantes() {
        ArrayList<Restaurante> restaurantes = new ArrayList<>();
        String[] SELECT = {"nombre", "descripcion", "categoria", "direccion", "coordenada1", "coordenada2"};

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLA_RESTAURANTES, SELECT, null, null, null, null,
                null);

        while (cursor.moveToNext()) {
            Restaurante restaurante = new Restaurante();
            restaurante.setNombre(cursor.getString(0));
            restaurante.setDescripcion(cursor.getString(1));
            restaurante.setCategoria(cursor.getString(2));
            restaurante.setUrl(cursor.getString(3));
            Double[] coor = new Double[]{
                    cursor.getDouble(4),
                    cursor.getDouble(5)
            };
            restaurante.setCoordenadas(coor);
            restaurantes.add(restaurante);
        }
        cursor.close();

        return restaurantes;
    }

    public void eliminarRestaurante(Restaurante restaurante) {
        SQLiteDatabase db=null;
        try{
            db = getReadableDatabase();
            db.delete(TABLA_RESTAURANTES, "nombre"+"= ?",new String[]{restaurante.getNombre()});
        }catch (SQLiteConstraintException e){

        }finally {
            if (db != null)
                db.close();
        }

    }

    public boolean validarFavorito(String nombre) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c=db.rawQuery("SELECT COUNT(*) FROM "+TABLA_RESTAURANTES+" WHERE nombre = ?",new String[]{nombre});
        c.moveToFirst();
        if (c.getInt(0)==0){
            db.close();
            return false;
        }
        db.close();
        return true;
    }
}
