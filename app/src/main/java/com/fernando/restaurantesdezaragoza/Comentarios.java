package com.fernando.restaurantesdezaragoza;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.fasterxml.jackson.databind.deser.std.StringArrayDeserializer;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Comentarios extends AppCompatActivity implements View.OnClickListener {
    private OpinionAdapter adapter;
    private List<Opinion> listaOpiniones;
    private String restaurante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentarios);

        Intent intent = getIntent();
        restaurante = intent.getStringExtra("restaurante");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Button btAnadirComentario = (Button) findViewById(R.id.btNuevo);
        btAnadirComentario.setOnClickListener(this);

        listaOpiniones = new ArrayList<>();
        adapter = new OpinionAdapter(this, listaOpiniones);
        ListView lvComentarios = (ListView) findViewById(R.id.lvComentarios);
        lvComentarios.setAdapter(adapter);

        WebService webservice = new WebService();
        webservice.execute();
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btNuevo){
            Intent intent = new Intent(this, NuevoComentario.class);

            intent.putExtra("Restaurante", restaurante);
            startActivity(intent);
        }
    }

    private class WebService extends AsyncTask<String, Void, Void> {

        private ProgressDialog dialog;

        @Override
        protected Void doInBackground(String... params) {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            Opinion[] opinionesArray = restTemplate.getForObject(Constantes.URL+"/opiniones", Opinion[].class);
            listaOpiniones.addAll(Arrays.asList(opinionesArray));

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listaOpiniones.clear();

            dialog = new ProgressDialog(Comentarios.this);
            dialog.setTitle(R.string.cargando);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (dialog!= null)
                dialog.dismiss();
            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            adapter.notifyDataSetChanged();
        }
    }
}
